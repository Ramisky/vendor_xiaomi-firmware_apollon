PRODUCT_COPY_FILES += \
        vendor/xiaomi-firmware/apollon/firmware-update/abl.elf:install/firmware-update/abl.elf \
        vendor/xiaomi-firmware/apollon/firmware-update/aop.mbn:install/firmware-update/aop.mbn \
        vendor/xiaomi-firmware/apollon/firmware-update/BTFM.bin:install/firmware-update/BTFM.bin \
        vendor/xiaomi-firmware/apollon/firmware-update/cmnlib64.mbn:install/firmware-update/cmnlib64.mbn \
        vendor/xiaomi-firmware/apollon/firmware-update/cmnlib.mbn:install/firmware-update/cmnlib.mbn \
        vendor/xiaomi-firmware/apollon/firmware-update/devcfg.mbn:install/firmware-update/devcfg.mbn \
        vendor/xiaomi-firmware/apollon/firmware-update/dspso.bin:install/firmware-update/dspso.bin \
        vendor/xiaomi-firmware/apollon/firmware-update/featenabler.mbn:install/firmware-update/featenabler.mbn \
        vendor/xiaomi-firmware/apollon/firmware-update/hyp.mbn:install/firmware-update/hyp.mbn \
        vendor/xiaomi-firmware/apollon/firmware-update/km4.mbn:install/firmware-update/km4.mbn \
        vendor/xiaomi-firmware/apollon/firmware-update/NON-HLOS.bin:install/firmware-update/NON-HLOS.bin \
        vendor/xiaomi-firmware/apollon/firmware-update/qupv3fw.elf:install/firmware-update/qupv3fw.elf \
        vendor/xiaomi-firmware/apollon/firmware-update/storsec.mbn:install/firmware-update/storsec.mbn \
        vendor/xiaomi-firmware/apollon/firmware-update/tz.mbn:install/firmware-update/tz.mbn \
        vendor/xiaomi-firmware/apollon/firmware-update/uefi_sec.mbn:install/firmware-update/uefi_sec.mbn \
        vendor/xiaomi-firmware/apollon/firmware-update/xbl_4.elf:install/firmware-update/xbl_4.elf \
        vendor/xiaomi-firmware/apollon/firmware-update/xbl_5.elf:install/firmware-update/xbl_5.elf \
        vendor/xiaomi-firmware/apollon/firmware-update/xbl_config_4.elf:install/firmware-update/xbl_config_4.elf \
        vendor/xiaomi-firmware/apollon/firmware-update/xbl_config_5.elf:install/firmware-update/xbl_config_5.elf
